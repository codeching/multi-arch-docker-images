FROM node:12.22.6

WORKDIR /usr/app

COPY ./package*.json ./

COPY ./ ./

RUN npm ci

CMD ["node","index.js"]