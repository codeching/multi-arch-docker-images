const express = require("express");
const app = express();
const port = 8080;

const bodyParser = require("body-parser");
app.use(bodyParser.json());

app.get("*", (req, res) => {
  res.send("Backend is running");
});

app.patch("*", (req, res) => {
  console.log("REQUEST INFO");
  console.log(
    "--------------------------------BODY-----------------------------------"
  );
  console.log(req.body);

  console.log(
    "--------------------------------HEADER-----------------------------------"
  );
  console.log(req.headers);

  res
    .status(200)
    .json({ text: "REQUEST INFO SENT BACK", req: JSON.stringify(req.body) });
});

app.listen(port, () => {
  console.log(`Backend application is listening on port ${port}`);
});
